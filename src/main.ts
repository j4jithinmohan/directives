import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { NgIfDirectiveModule } from './app/Module/Directives/ngIfDirectiveModule';

platformBrowserDynamic().bootstrapModule(NgIfDirectiveModule);
