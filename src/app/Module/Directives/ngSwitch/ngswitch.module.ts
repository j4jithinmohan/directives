import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {NgSwitchComponent} from '../../../Component/Directives/ngSwitch/ngswitch.component';

@NgModule({
    imports: [BrowserModule],
    declarations: [NgSwitchComponent],
    bootstrap: [NgSwitchComponent]
})

export class NgForSwitchModule{}