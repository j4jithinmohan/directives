import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {NgForDirectiveComponent} from '../../../Component/Directives/ngFor/ngForDirectiveComponent';

@NgModule({
    imports: [BrowserModule],
    declarations: [NgForDirectiveComponent],
    bootstrap: [NgForDirectiveComponent]
})

export class NgForDirectiveModule{}