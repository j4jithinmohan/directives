import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';

import {NgIfDirectiveComponent} from '../../Component/Directives/ngIfDirectiveComponent';
import {NgForDirectiveComponent} from '../../Component/Directives/ngFor/ngForDirectiveComponent';
import {NgSwitchComponent} from '../../Component/Directives/ngSwitch/ngswitch.component';

@NgModule({
    imports: [BrowserModule,FormsModule],
    declarations: [NgIfDirectiveComponent,NgForDirectiveComponent,NgSwitchComponent],
    bootstrap: [NgIfDirectiveComponent,NgForDirectiveComponent,NgSwitchComponent]
})

export class NgIfDirectiveModule{}