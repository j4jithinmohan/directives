import { Component } from '@angular/core';

@Component({
    selector: 'ng-switch',
    templateUrl: './View/ngswitch.html'
})

export class NgSwitchComponent {
    public choice = 'default';
}