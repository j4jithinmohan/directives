import {Component} from '@angular/core';

@Component({
    selector: 'my-list',
    templateUrl: './View/mylist.html'   
})

export class NgForDirectiveComponent{
    myList: any[] = ['One', 'Two', 'Three', 'Four', 'Five'];
}