import {Component} from '@angular/core';

@Component({
    selector: 'ngif-item',
    template: '<h3 *ngIf="status">Checking {{title}} directive</h3>'
})

export class NgIfDirectiveComponent{
    title:string = 'ngIf';
    status:boolean = true;
}